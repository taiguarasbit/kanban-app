import React, { Component } from 'react';
import { Board } from 'react-trello';

const data = {
  lanes: [
    {
      id: 'lane1',
      title: 'Do',
      label: '2/2',
      cards: [
        {id: 'Card1', title: 'Task 1', description: 'Long Long Long Description Task 1', label: '30 mins'},
        {id: 'Card2', title: 'Task 2', description: 'Long Long Long Description Task 2', label: '5 mins', metadata: {sha: 'be312a1'}}
      ]
    },
    {
      id: 'lane2',
      title: 'Doing',
      label: '2/2',
      cards: [
        {id: 'Card3', title: 'Task 3', description: 'Long Long Long Description Task 3', label: '25 mins'},
        {id: 'Card4', title: 'Task 4', description: 'Long Long Long Description Task 4', label: '10 mins'}
      ]
    },
    {
      id: 'lane3',
      title: 'Done',
      label: '1/1',
      cards: [
        {id: 'Card5', title: 'Task 5', description: 'Long Long Long Description Task 5', label: '55 mins'}
        
    ]
    }
  ]
}

class App extends Component {
  render() {
    return (
      <div className="App">
      <Board data={data} draggable={true} editable={true} />        
      </div>
    );
  }
}

export default App;
